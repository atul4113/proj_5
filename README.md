# Django and Vue.JS
1. Clone project using 
# using HTTPS
git clone https://gitlab.com/atul4113/proj_5.git

or

# using SSH
git clone git@gitlab.com:atul4113/proj_5.git

2.After clone Create Virtual ENV
python -m venv env_name

3. Activate VENV
--in ubuntu
source env_name/bin/activate hit Enter

--in windows
env_name\Scripts\activate hit Enter

4. After activation install required python libraries using
pip install -r djangovuejs/requirements.txt hit Enter

5. Move on djangovuejs
cd djangovuejs

6. python manage.py runserver
7. Open Web Browser 127.0.0.1
